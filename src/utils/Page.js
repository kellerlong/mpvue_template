import Vue from 'vue'
import Router from './Router'
import Vuex from 'vuex'

export default class Page {
  constructor (App, Store) {
    // 初始化路由
    this.initRouter()
    // 初始化vuex
    Store && this.initStore(Store)
    // 初始化VUE
    this.app = new Vue(App)
    this.app.$mount()
  }

  initRouter () {
    Vue.use(Vue => {
      Vue.prototype.$router = new Router()
    })
  }

  initStore (Store) {
    Vue.use(Vuex)
    Vue.use(Vue => {
      Vue.prototype.$store = new Vuex.Store(Store)
    })
  }

  getApp () {
    return this.app
  }
}
