const createNote = (type) => {
  return (object, key, content) => {
    object[type] = { ...object[type], [key]: content.value }
  }
}

export const State = (options) => {
  return (object, key) => {
    object.state = { ...object.state || {}, [key]: options.default }
    if (options.set) {
      // 新增设置该属性的 Actions
      const setFun = (state, data) => {
        state[key] = data
        return { ...state }
      }
      const name = key.replace(/\b(\w)/g, (val) => val.toUpperCase())
      object.mutations = { ...object.mutations || {}, [`set${name}`]: setFun }
    }
  }
}

export const Store = (Obj) => {
  const object = new Obj()
  const newObj = {
    ...object, // 防止TS报错  其实原型连浅拷贝原因 这个加上去什么都没有
    namespace: Obj.name,
    state: object.state || {},
    actions: object.actions || {},
    effects: object.effects || {},
    mutations: object.mutations || {}
  }
  return newObj
}

export const Action = createNote('actions')
// export const Effect = createNote('effects')
export const Mutation = createNote('mutations')
