export default class Router {
  push (url) {
    wx.navigateTo({url})
  }

  replace (url) {
    wx.redirectTo({url})
  }

  go () {
  }

  reLaunch (url) {
    wx.reLaunch({url})
  }
  // 返回上一页
  back () {
    wx.navigateBack()
  }
}
