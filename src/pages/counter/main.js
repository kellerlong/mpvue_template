import App from './index'
import store from './store'
import Page from '../../utils/Page'

const app = new Page(App, store)
