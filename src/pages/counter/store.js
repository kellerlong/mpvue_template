import {State, Store, Mutation, Action} from '../../utils/store-decorator'
import {getHome} from '../../services/homeService'

// 复杂数据 可以定义一个新文件存储 这里只是一个demo
const testDataDefault = {
  I: {
    BirthdayFormat: ''
  }
}
@Store
export default class {
  @State({default: 0})
  count = 0
  // set 为true 会自动创建一个 setTestData的Mutation
  @State({default: testDataDefault, set: true})
  testData = {
    I: {
      BirthdayFormat: ''
    }
  }

  @Mutation
  increment (state) {
    const obj = state
    obj.count += 1
  }

  @Mutation
  decrement (state) {
    const obj = state
    obj.count -= 1
  }

  @Action
  async getHome ({ commit }, payload) {
    const data = await getHome()
    commit('setTestData', data)
  }
}
