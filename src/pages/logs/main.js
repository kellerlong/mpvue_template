
import App from './index'
import Page from '../../utils/Page'

const app = new Page(App)

export default {
  config: {
    navigationBarTitleText: '查看启动日志'
  }
}
